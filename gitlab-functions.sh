GITLAB_WRITE_ACCESS_ENABLED=0

gitlab()
{
        local end_point="$1"
        local params="$2"
        local primary_key="$3"
        local -n array="$4"
        local expected_keys=($5)
        local max_results="${6:-9999}"
        local results_per_page=${7:-100}
        local flatten_scriptlet="$8"
        local array_space="$9"

        local current_page=1
        local fetched_results=0
        local total_fetched=0

        while [ "$total_fetched" -lt "$max_results" ]; do
                input_file=$(mktemp -p "${STATE_DIR}/" "curl${end_point//\//-}-XXXXX.json")
                output_file=$(mktemp -p "${STATE_DIR}/" "jq${end_point//\//-}-XXXXX.json")

                curl --header "PRIVATE-TOKEN: ${TOKEN}" "${URL}${end_point}?page=${current_page}&${params// /\&}&per_page=${results_per_page}" 2> /dev/null > "${input_file}"

                [ -z "${flatten_scriptlet}" ] && flatten_scriptlet="."

                jq_flattening_script='((select(type == "object") | [.]), (select(type == "array") | .))
                                      | to_entries[]
                                      | .value.'${primary_key}' as $primary_key
                                      | .key = $primary_key
                                      | .value
                                      | '${flatten_scriptlet}'
                                      | to_entries
                                      | to_entries[]
                                      | .value.key = ($primary_key | tostring) + " " + .value.key
                                      | [.value]
                                      | from_entries'
                jq_formatting_script='add
                                      | ((select(type == "null") | []), (select(type != "null") | .))
                                      | to_entries
                                      | map((.key) + ":" + (.value | tostring))
                                      | @tsv'

                cat "${input_file}" | jq "${jq_flattening_script}"                             \
                                    | jq -r -s "${jq_formatting_script}" | tr '\t' '\n'        \
                                    > "${output_file}"

                while IFS=":" read key value
                do
                        key_parts=(${key})
                        namespace="${key_parts[0]}"
                        key_name="${key_parts[1]}"

                        if [ "${key_name}" = "${primary_key}" ]
                        then
                                echo "${namespace}"
                                fetched_results=$((fetched_results + 1))
                        fi

                        for expected_key in "${expected_keys[@]}"
                        do
                                if [ "${key_name}" = "${expected_key}" ]
                                then
                                        array["${array_space:+${array_space} }${key}"]="$value"
                                        break
                                fi
                        done

                done < "${output_file}"

                total_fetched=$((total_fetched + fetched_results))
                if [ "$fetched_results" -lt "$results_per_page" ]; then
                        break
                fi

                fetched_results=0
                current_page=$((current_page + 1))

                if [ "${DEBUG}" != "true" ]
                then
                        rm -f "${input_file}" "${output_file}"
                fi
        done
}

look_up_gitlab_group()
{
       local id="$1"
       local -n result="$2"
       local expected_keys="$3"

       gitlab "/groups/${id}" "" "id" result "${expected_keys}" > /dev/null
}

look_up_gitlab_group_projects()
{
        id="$1"
        local -n result="$2"
        expected_keys="$3"
        primary_key="$4"

        gitlab "/groups/${id}/projects" "" "id" result "${expected_keys}"
}

look_up_gitlab_confidential_project_issues()
{
        id="$1"
        local -n result="$2"
        expected_keys="$3"

        gitlab "/projects/${id}/issues" "confidential=true state=opened" "id" result "${expected_keys}" "" 1
}

fork_gitlab_group_project_privately()
{
        local id="$1"
        local new_group="$2"
        local -n result="$3"
        expected_keys="$4"

        gitlab "/projects/fork/${id}" "namespace_id=${new_group} visibility=private" "id" result "${expected_keys}" > /dev/null
}
